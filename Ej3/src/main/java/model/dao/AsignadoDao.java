package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import model.conexion.Conexion;
import model.dto.Asignado;

public class AsignadoDao {

	public void registrarAsignacion(Asignado miAsignacion)
	{
		Conexion conex= new Conexion();
		
		try {
			Statement st = conex.getConnection().createStatement();
			String sql= "INSERT INTO Asignado_A VALUES ('"+miAsignacion.getCientifico()+"', '"
					+miAsignacion.getProyecto()+"');";
			st.executeUpdate(sql);
			JOptionPane.showMessageDialog(null, "Se ha registrado Exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
			System.out.println(sql);
			st.close();
			conex.desconectar();
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Registro");
		}
	}
	
	public ArrayList<Asignado> buscarAsignado(String id,int dni,boolean b) 
	{
		Conexion conex= new Conexion();
		ArrayList<Asignado> asignado= new ArrayList<Asignado>();
		boolean existe=false;
		int index=0;
		String sql="";
		try {
			if(b) sql = "SELECT * FROM Asignado_A where proyecto = ? ";
			else sql = "SELECT * FROM Asignado_A where cientifico = ? ";
			PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
			if(b) consulta.setString(1, id);
			else consulta.setInt(1, dni);
			ResultSet res = consulta.executeQuery();
			while(res.next()){
				asignado.add(new Asignado());
				existe=true;
				asignado.get(index).setProyecto(res.getString("proyecto"));
				asignado.get(index).setCientifico(res.getString("cientifico"));
				index++;
			}
			res.close();
			conex.desconectar();
			System.out.println(sql);
					
			} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Error, no se conecto");
					System.out.println(e);
			}
		
			if (existe) {
				return asignado;
			}
			else return null;				
	}
	
	public Asignado buscarAsignado(Asignado miAsignacion) 
	{
		Conexion conex= new Conexion();
		Asignado asignado= new Asignado();
		boolean existe=false;
		String sql="";
		try {
			sql = "SELECT * FROM Asignado_A WHERE proyecto = ? and cientifico = ? ";
			PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
			consulta.setString(1, miAsignacion.getProyecto());
			consulta.setString(2, miAsignacion.getCientifico());
			ResultSet res = consulta.executeQuery();
			while(res.next()){
				existe=true;
				asignado.setProyecto(res.getString("proyecto"));
				asignado.setCientifico(res.getString("cientifico"));
			}
			res.close();
			conex.desconectar();
			System.out.println(sql);
					
			} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Error, no se conecto");
					System.out.println(e);
			}
		
			if (existe) {
				return asignado;
			}
			else return null;				
	}
	
	public void modificarAsignado(Asignado miAsignacion) {
		
		Conexion conex= new Conexion();
		try{
			String consulta="UPDATE Asignado_A SET proyecto= ? ,cientifico = ? WHERE proyecto = ? and cientifico = ? ";
			PreparedStatement estatuto = conex.getConnection().prepareStatement(consulta);
			
            estatuto.setString(1, miAsignacion.getProyecto());
            estatuto.setString(2, miAsignacion.getCientifico());
            estatuto.setString(3, miAsignacion.getProyecto());
            estatuto.setString(4, miAsignacion.getCientifico());
            estatuto.executeUpdate();
            
          JOptionPane.showMessageDialog(null, " Se ha Modificado Correctamente ","Confirmación",JOptionPane.INFORMATION_MESSAGE);
          System.out.println(consulta);
         

        }catch(SQLException	 e){

            System.out.println(e);
            JOptionPane.showMessageDialog(null, "Error al Modificar","Error",JOptionPane.ERROR_MESSAGE);

        }
	}
	
	public void eliminarAsignado(Asignado miAsignacion)
	{
		Conexion conex= new Conexion();
		try {
			String sql= "DELETE FROM Asignado_A WHERE cientifico= ? and proyecto= ? ";
			PreparedStatement consulta = conex.getConnection().prepareStatement(sql);
			consulta.setString(1, miAsignacion.getCientifico());
			consulta.setString(2, miAsignacion.getProyecto());
			int error=consulta.executeUpdate();
			if(error!=0) {
	            JOptionPane.showMessageDialog(null, " Se ha Eliminado Correctamente","Información",JOptionPane.INFORMATION_MESSAGE);
	            System.out.println(sql);
			}else JOptionPane.showMessageDialog(null, "El codigo no existe para ningun cliente."); 
			conex.desconectar();
			
			
		} catch (SQLException e) {
            System.out.println(e.getMessage());
			JOptionPane.showMessageDialog(null, "No se Elimino");
		}
	}
}
