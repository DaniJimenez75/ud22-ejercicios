package model.service;

import javax.swing.JOptionPane;

import controller.ProyectoController;
import model.dao.ProyectoDao;
import model.dto.Proyecto;

public class ProyectoServ {
	private ProyectoController proyectoController;
	public static boolean consultaProyecto=false;
	public static boolean modificaProyecto=false;
	
	
	
	/**
	 * @return the proyectoController
	 */
	public ProyectoController getProyectoController() {
		return proyectoController;
	}



	/**
	 * @param proyectoController the proyectoController to set
	 */
	public void setProyectoController(ProyectoController proyectoController) {
		this.proyectoController = proyectoController;
	}



	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Proyecto miProyecto) {
		
		if(!existeProyecto(miProyecto.getId())) {
			ProyectoDao miProyectoDao = new ProyectoDao();
			miProyectoDao.registrarProyecto(miProyecto);
		}else {
			JOptionPane.showMessageDialog(null,"Ya existe el cientifico con dni: "+miProyecto.getId(),"Advertencia",JOptionPane.WARNING_MESSAGE);
		}					
	}
	
	
	
	public Proyecto validarConsulta(String codigoProyecto) {
		ProyectoDao miProyectoDao;
		
		try {
			if (codigoProyecto.length() <= 4) {
				miProyectoDao = new ProyectoDao();
				consultaProyecto=true;
				return miProyectoDao.buscarProyecto(codigoProyecto);						
			}else{
				JOptionPane.showMessageDialog(null,"El codigo del proyecto no puede estar vacio.","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultaProyecto=false;
			}
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaProyecto=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaProyecto=false;
		}
					
		return null;
	}
	
	//Metodo que valida los datos de ModificaciÃ³n antes de pasar estos al DAO
    public void validarModificacion(Proyecto miProyecto) {
        ProyectoDao miProyectoDao;
        if (existeProyecto(miProyecto.getId())) {
            miProyectoDao = new ProyectoDao();
            miProyectoDao.modificarProyecto(miProyecto);
            modificaProyecto=true;
        }else{
            JOptionPane.showMessageDialog(null,"No existe el proyecto con id: "+miProyecto.getId(),"Advertencia",JOptionPane.WARNING_MESSAGE);
            modificaProyecto=false;
        }
    }
    
	
    //Metodo que valida los datos de EliminaciÃ³n antes de pasar estos al DAO
	public void validarEliminacion(String codigoProyecto) {
		ProyectoDao miProyectoDao=new ProyectoDao();
		miProyectoDao.eliminarProyecto(codigoProyecto);
		
	}
	
	public boolean existeProyecto(String proyecto_id) {
		
		ProyectoServ ps = new ProyectoServ();
		
		if (ps.validarConsulta(proyecto_id) != null) {
			return true;
		}
		return false;
	}
}
