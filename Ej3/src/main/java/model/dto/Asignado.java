package model.dto;

public class Asignado {
	private String Cientifico;
	private String Proyecto;
	
	//GETTERS SETTERS
	public String getCientifico() {
		return Cientifico;
	}
	public void setCientifico(String cientifico) {
		Cientifico = cientifico;
	}
	public String getProyecto() {
		return Proyecto;
	}
	public void setProyecto(String proyecto) {
		Proyecto = proyecto;
	}
	
	
}
