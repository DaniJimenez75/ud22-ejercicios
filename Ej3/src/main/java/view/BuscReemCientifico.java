package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.CientificoController;
import model.dto.Cientifico;

public class BuscReemCientifico extends JFrame {

	private JPanel contentPane;
	private JTextField nomApelsTexto;
	private JTextField dniTexto;
	private JButton actualizarButton;
	private CientificoController cientificoController;



	

	/**
	 * Create the frame.
	 */
	public BuscReemCientifico() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("DNI:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(216, 29, 28, 14);
		contentPane.add(lblNewLabel);
		

		
		JLabel lblNombre = new JLabel("NomApels:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombre.setBounds(161, 101, 68, 14);
		contentPane.add(lblNombre);

		
		actualizarButton = new JButton("Actualizar");
		actualizarButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		actualizarButton.setBounds(232, 234, 180, 70);
		actualizarButton.addActionListener(buscarReemplazar);
		actualizarButton.setEnabled(false);
		contentPane.add(actualizarButton);
		
		JButton buscarButton = new JButton("Buscar por DNI");
		buscarButton.setFont(new Font("Tahoma", Font.PLAIN, 17));
		buscarButton.setBounds(353, 29, 146, 20);
		buscarButton.addActionListener(buscarReemplazar);
		contentPane.add(buscarButton);
		
		JButton btnNewButton_1 = new JButton("<---");
		btnNewButton_1.setBounds(12, 13, 76, 25);
		btnNewButton_1.addActionListener(buscarReemplazar);
		contentPane.add(btnNewButton_1);
		
		nomApelsTexto = new JTextField();
		nomApelsTexto.setBounds(249, 100, 86, 20);
		contentPane.add(nomApelsTexto);
		nomApelsTexto.setColumns(10);
		
		dniTexto = new JTextField();
		dniTexto.setColumns(10);
		dniTexto.setBounds(249, 28, 86, 20);
		contentPane.add(dniTexto);
	}

ActionListener buscarReemplazar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			switch (aux.getText()) {
			case "<---":
				cientificoController.mostrarMenuPrincipal();
				dispose();
				break;
			case "Buscar por DNI":
				buscarCientifico();
				break;
				
			case "Actualizar":
				modificarCientifico();
				break;
			}
			
		}
	};
	
	private void buscarCientifico() {
		Cientifico miCientifico = new Cientifico();
			if(dniTexto.getText().length()>0) {
				miCientifico = cientificoController.buscarCientifico(dniTexto.getText());
				if(miCientifico != null) {
					dniTexto.setEnabled(false);
					nomApelsTexto.setEnabled(true);
					nomApelsTexto.setText(miCientifico.getNomApels());
					actualizarButton.setEnabled(true);

				}else JOptionPane.showMessageDialog(this.contentPane, "Usuario no encontrado.");
			}
	}
	
	private void modificarCientifico() {
		Cientifico miCientifico = new Cientifico();
			
		//Guardo nueva info
		miCientifico.setDni(dniTexto.getText());
		miCientifico.setNomApels(nomApelsTexto.getText());


		
		//Actualizo datos
		cientificoController.modificarCientifico(miCientifico);
		if(cientificoController.getCientificoServ().modificaCientifico) {
			nomApelsTexto.setEnabled(false);
			nomApelsTexto.setText("");
			dniTexto.setEnabled(true);
			actualizarButton.setEnabled(false);
		}
		
		
	}

	public void setCientificoController(CientificoController cientificoController) {
		this.cientificoController = cientificoController;
	}
	
	

}
