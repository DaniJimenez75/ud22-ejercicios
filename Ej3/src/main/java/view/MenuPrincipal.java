package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.AsignadoController;
import controller.CientificoController;
import controller.ProyectoController;

public class MenuPrincipal extends JFrame {

	private JPanel contentPane;

	AsignadoController asignadoController;
	CientificoController cientificoController;
	ProyectoController proyectoController;
	
	

	/**
	 * Create the frame.
	 */
	public MenuPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1054, 546);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton menuCrearCientifico = new JButton("Crear cientifico");
		menuCrearCientifico.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuCrearCientifico.setBounds(27, 142, 166, 70);
		menuCrearCientifico.addActionListener(nuevaVentana);
		contentPane.add(menuCrearCientifico);
		
		JButton menuEliminarCientifico = new JButton("Eliminar cientifico");
		menuEliminarCientifico.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuEliminarCientifico.setBounds(27, 337, 191, 70);
		menuEliminarCientifico.addActionListener(nuevaVentana);
		contentPane.add(menuEliminarCientifico);
		
		JLabel lblNewLabel = new JLabel("Selecciona una opción");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(373, 70, 229, 25);
		contentPane.add(lblNewLabel);
		
		JLabel lblGestinDeBd = new JLabel("GESTIÓN DE BD");
		lblGestinDeBd.setFont(new Font("Rockwell Condensed", Font.PLAIN, 22));
		lblGestinDeBd.setBounds(402, 11, 229, 25);
		contentPane.add(lblGestinDeBd);
		
		JButton menuBuscarActualizarCientifico = new JButton("Buscar y Actualizar cientifico");
		menuBuscarActualizarCientifico.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuBuscarActualizarCientifico.setBounds(26, 235, 305, 70);
		menuBuscarActualizarCientifico.addActionListener(nuevaVentana);
		contentPane.add(menuBuscarActualizarCientifico);
		
		JButton menuCrearAsignacion = new JButton("Crear asignación");
		menuCrearAsignacion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuCrearAsignacion.setBounds(369, 143, 214, 70);
		menuCrearAsignacion.addActionListener(nuevaVentana);
		contentPane.add(menuCrearAsignacion);
		
		JButton menuBuscarActualizarAsignacion = new JButton("Buscar y Actualizar asignación");
		menuBuscarActualizarAsignacion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuBuscarActualizarAsignacion.setBounds(369, 236, 302, 70);
		menuBuscarActualizarAsignacion.addActionListener(nuevaVentana);
		contentPane.add(menuBuscarActualizarAsignacion);
		
		JButton menuEliminarAsignacion = new JButton("Eliminar asignación");
		menuEliminarAsignacion.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuEliminarAsignacion.setBounds(369, 338, 214, 70);
		menuEliminarAsignacion.addActionListener(nuevaVentana);
		contentPane.add(menuEliminarAsignacion);
		
		JLabel lblClientes = new JLabel("Cientificos");
		lblClientes.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblClientes.setBounds(80, 106, 91, 25);	
		contentPane.add(lblClientes);
		
		JLabel lblVideos = new JLabel("Asignaciones");
		lblVideos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblVideos.setBounds(413, 106, 153, 25);
		contentPane.add(lblVideos);
		
		JLabel lblProyecto = new JLabel("Proyecto");
		lblProyecto.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblProyecto.setBounds(764, 106, 153, 25);
		contentPane.add(lblProyecto);
		
		JButton menuCrearProyecto = new JButton("Crear proyecto");
		menuCrearProyecto.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuCrearProyecto.setBounds(720, 143, 166, 70);
		menuCrearProyecto.addActionListener(nuevaVentana);
		contentPane.add(menuCrearProyecto);
		
		JButton menuBuscarYActualizar = new JButton("Buscar y Actualizar proyecto");
		menuBuscarYActualizar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuBuscarYActualizar.setBounds(720, 236, 296, 70);
		menuBuscarYActualizar.addActionListener(nuevaVentana);
		contentPane.add(menuBuscarYActualizar);
		
		JButton menuEliminarProyecto = new JButton("Eliminar proyecto");
		menuEliminarProyecto.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuEliminarProyecto.setBounds(720, 338, 214, 70);
		menuEliminarProyecto.addActionListener(nuevaVentana);
		contentPane.add(menuEliminarProyecto);
	}
	
ActionListener nuevaVentana = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			switch(aux.getText()) {
			case "Crear cientifico":
				dispose();
				cientificoController.mostrarVentanaCreacion();
				break;				
			case "Eliminar cientifico":
				dispose();
				cientificoController.mostrarVentanaEliminar();
				break;				
			case "Buscar y Actualizar cientifico":
				dispose();
				cientificoController.mostrarVentanaBusActu();
				break;
			case "Crear asignación":
				dispose();
				asignadoController.mostrarVentanaCreacion();
				break;
			case "Eliminar asignación":
				dispose();
				asignadoController.mostrarVentanaEliminar();
				break;
			case "Buscar y Actualizar asignación":
				dispose();
				asignadoController.mostrarVentanaBusActu();
				break;
			case "Crear proyecto":
				dispose();
				proyectoController.mostrarVentanaCreacion();
				break;
			case "Eliminar proyecto":
				dispose();
				proyectoController.mostrarVentanaEliminar();
				break;
			case "Buscar y Actualizar proyecto":
				dispose();
				proyectoController.mostrarVentanaBusActu();
				break;
			}
		}
	};



public void setAsignadoController(AsignadoController asignadoController) {
	this.asignadoController = asignadoController;
}

public void setCientificoController(CientificoController cientificoController) {
	this.cientificoController = cientificoController;
}

public void setProyectoController(ProyectoController proyectoController) {
	this.proyectoController = proyectoController;
}
	
	


}
