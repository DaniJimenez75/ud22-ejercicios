package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.AsignadoController;
import controller.CientificoController;
import controller.ProyectoController;
import model.dto.Asignado;
import model.dto.Proyecto;

public class CrearAsignacion extends JFrame {

	private JPanel contentPane;
	private JTextField cientificoTexto;
	private JTextField proyectoTexto;
	private AsignadoController asignadoController;


	/**
	 * Launch the application.
	 */
	
	

	/**
	 * Create the frame.
	 */
	public CrearAsignacion() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton crearButton = new JButton("Crear");
		crearButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		crearButton.setBounds(210, 233, 180, 70);
		crearButton.addActionListener(crear);
		contentPane.add(crearButton);
		
		JButton btnNewButton_1 = new JButton("<---");
		btnNewButton_1.setBounds(12, 13, 76, 25);
		btnNewButton_1.addActionListener(crear);
		contentPane.add(btnNewButton_1);
		
		JLabel lblNewLabel = new JLabel("Cientifico:");
		lblNewLabel.setBounds(113, 77, 61, 14);
		contentPane.add(lblNewLabel);
		
		
		JLabel lblNomapels = new JLabel("Proyecto:");
		lblNomapels.setBounds(278, 77, 67, 14);
		contentPane.add(lblNomapels);
		
		cientificoTexto = new JTextField();
		cientificoTexto.setBounds(167, 74, 86, 20);
		contentPane.add(cientificoTexto);
		cientificoTexto.setColumns(10);
		
		proyectoTexto = new JTextField();
		proyectoTexto.setColumns(10);
		proyectoTexto.setBounds(334, 74, 86, 20);
		contentPane.add(proyectoTexto);
	}
	
ActionListener crear = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			if (aux.getText().equals("Crear"))
			{
				try {
					Asignado miAsignacion =new Asignado();
					miAsignacion.setCientifico(cientificoTexto.getText());
					miAsignacion.setProyecto(proyectoTexto.getText());



					
					asignadoController.registrarAsignacion(miAsignacion);	
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
					System.out.println(ex);
				}
			}
			if (aux.getText().equals("<---"))
			{
				asignadoController.mostrarMenuPrincipal();
				dispose();
			} 
			
		}
	};


public void setAsignadoController(AsignadoController asignadoController) {
	this.asignadoController = asignadoController;
}
	
	

}
