package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.AsignadoController;
import model.dto.Asignado;
import model.dto.Cientifico;

public class BuscReemAsignacion extends JFrame {

	private JPanel contentPane;
	private JTextField cientificoTexto;
	private JTextField proyectoTexto;
	private JButton actualizarButton;
	private AsignadoController asignadoController;
	

	/**
	 * Create the frame.
	 */
	public BuscReemAsignacion() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cientifico:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(189, 29, 68, 14);
		contentPane.add(lblNewLabel);		
		
		JLabel lblNombre = new JLabel("Proyecto:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombre.setBounds(189, 102, 68, 14);
		contentPane.add(lblNombre);

		
		actualizarButton = new JButton("Actualizar");
		actualizarButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		actualizarButton.setBounds(232, 234, 180, 70);
		actualizarButton.addActionListener(buscarReemplazar);
		actualizarButton.setEnabled(false);
		contentPane.add(actualizarButton);
		
		JButton buscarButton = new JButton("Buscar por Cientifico");
		buscarButton.setFont(new Font("Tahoma", Font.PLAIN, 17));
		buscarButton.setBounds(353, 29, 198, 20);
		buscarButton.addActionListener(buscarReemplazar);
		contentPane.add(buscarButton);
		
		JButton btnNewButton_1 = new JButton("<---");
		btnNewButton_1.setBounds(12, 13, 76, 25);
		btnNewButton_1.addActionListener(buscarReemplazar);
		contentPane.add(btnNewButton_1);
		
		
		cientificoTexto = new JTextField();
		cientificoTexto.setBounds(255, 28, 86, 20);
		contentPane.add(cientificoTexto);
		cientificoTexto.setColumns(10);
		
		proyectoTexto = new JTextField();
		proyectoTexto.setColumns(10);
		proyectoTexto.setBounds(255, 100, 86, 20);
		contentPane.add(proyectoTexto);
	}
	
ActionListener buscarReemplazar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			switch (aux.getText()) {
			case "<---":
				asignadoController.mostrarMenuPrincipal();
				dispose();
				break;
			case "Buscar por Cientifico":
				buscarAsignacion();
				break;
				
			case "Actualizar":
				modificarAsignacion();
				break;
			}
			
		}
	};
	
	private void buscarAsignacion() {
		ArrayList<Asignado> miAsignacion = new ArrayList<Asignado>();
			if(cientificoTexto.getText().length()>0) {
				miAsignacion = asignadoController.buscarAsignacionDNI(cientificoTexto.getText());
				if(miAsignacion.size() > 0) {
					cientificoTexto.setEnabled(false);
					proyectoTexto.setEnabled(true);
					proyectoTexto.setText(miAsignacion.get(0).getProyecto());
					actualizarButton.setEnabled(true);

				}else JOptionPane.showMessageDialog(this.contentPane, "Usuario no encontrado.");
			}
	}
	
	private void modificarAsignacion() {
		Asignado miAsignacion = new Asignado();
			
		//Guardo nueva info
		miAsignacion.setCientifico(cientificoTexto.getText());
		miAsignacion.setProyecto(proyectoTexto.getText());


		
		//Actualizo datos
		asignadoController.modificarAsignacion(miAsignacion);
		if(asignadoController.getAsignadoServ().modificaAsignacion) {
			proyectoTexto.setEnabled(false);
			proyectoTexto.setText("");
			cientificoTexto.setEnabled(true);
			actualizarButton.setEnabled(false);
		}
		
		
	}

	public void setAsignadoController(AsignadoController asignadoController) {
		this.asignadoController = asignadoController;
	}
	

}
