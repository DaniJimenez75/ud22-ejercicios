package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.ProyectoController;
import model.dto.Cientifico;
import model.dto.Proyecto;

public class CrearProyecto extends JFrame {

	private JPanel contentPane;
	private JTextField horasTexto;
	private JTextField idTexto;
	private JTextField nombreTexto;
	private ProyectoController proyectoController;

	
	

	/**
	 * Create the frame.
	 */
	public CrearProyecto() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton crearButton = new JButton("Crear");
		crearButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		crearButton.setBounds(210, 233, 180, 70);
		crearButton.addActionListener(crear);
		contentPane.add(crearButton);
		
		JButton btnNewButton_1 = new JButton("<---");
		btnNewButton_1.setBounds(12, 13, 76, 25);
		btnNewButton_1.addActionListener(crear);
		contentPane.add(btnNewButton_1);
		
		JLabel lblNewLabel = new JLabel("Id:");
		lblNewLabel.setBounds(113, 77, 46, 14);
		contentPane.add(lblNewLabel);
		

		JLabel lblNomapels = new JLabel("Nombre:");
		lblNomapels.setBounds(278, 77, 67, 14);
		contentPane.add(lblNomapels);
		
		JLabel lblHoras = new JLabel("Horas:");
		lblHoras.setBounds(113, 119, 67, 14);
		contentPane.add(lblHoras);
		
		horasTexto = new JTextField();
		horasTexto.setBounds(169, 116, 86, 20);
		contentPane.add(horasTexto);
		horasTexto.setColumns(10);
		
		idTexto = new JTextField();
		idTexto.setColumns(10);
		idTexto.setBounds(138, 74, 86, 20);
		contentPane.add(idTexto);
		
		nombreTexto = new JTextField();
		nombreTexto.setColumns(10);
		nombreTexto.setBounds(328, 74, 86, 20);
		contentPane.add(nombreTexto);
	}
	

	
ActionListener crear = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			if (aux.getText().equals("Crear"))
			{
				try {
					Proyecto miProyecto =new Proyecto();
					miProyecto.setId(idTexto.getText());
					miProyecto.setNombre(nombreTexto.getText());
					miProyecto.setHoras(Integer.parseInt(horasTexto.getText()));


					
					proyectoController.registrarProyecto(miProyecto);	
				} catch (Exception ex) {
					JOptionPane.showMessageDialog(null,"Error en el Ingreso de Datos","Error",JOptionPane.ERROR_MESSAGE);
					System.out.println(ex);
				}
			}
			if (aux.getText().equals("<---"))
			{
				proyectoController.mostrarMenuPrincipal();
				dispose();
			} 
			
		}
	};




public void setProyectoController(ProyectoController proyectoController) {
	this.proyectoController = proyectoController;
}
	
	
}
