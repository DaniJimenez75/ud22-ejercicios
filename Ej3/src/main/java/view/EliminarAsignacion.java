package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.AsignadoController;
import model.dto.Asignado;

public class EliminarAsignacion extends JFrame {

	private JPanel contentPane;
	private JTextField cientificoTexto;
	private AsignadoController asignadoController;

	

	/**
	 * Create the frame.
	 */
	public EliminarAsignacion() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("DNI Cientifico:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(84, 85, 102, 23);
		contentPane.add(lblNewLabel);

		
		JButton eliminarButton = new JButton("Eliminar");
		eliminarButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		eliminarButton.setBounds(242, 146, 180, 70);
		eliminarButton.addActionListener(eliminar);
		contentPane.add(eliminarButton);
		
		JButton btnNewButton_1 = new JButton("<---");
		btnNewButton_1.setBounds(12, 13, 76, 25);
		btnNewButton_1.addActionListener(eliminar);
		contentPane.add(btnNewButton_1);
		
		cientificoTexto = new JTextField();
		cientificoTexto.setBounds(198, 87, 86, 20);
		contentPane.add(cientificoTexto);
		cientificoTexto.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("ID Proyecto:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(329, 89, 86, 16);
		contentPane.add(lblNewLabel_1);
		
		ProyectoText = new JTextField();
		ProyectoText.setBounds(434, 86, 116, 22);
		contentPane.add(ProyectoText);
		ProyectoText.setColumns(10);
	}
	
ActionListener eliminar= new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			if (aux.getText().equals("Eliminar"))
			{
				if (!cientificoTexto.getText().equals(""))
				{
					int respuesta = JOptionPane.showConfirmDialog(null,
							"Esta seguro de eliminar esta asignación?", "Confirmación",
							JOptionPane.YES_NO_OPTION);
					if (respuesta == JOptionPane.YES_NO_OPTION)
					{
						Asignado asign = new Asignado();
						asign.setCientifico(cientificoTexto.getText());
						asign.setProyecto(ProyectoText.getText());
						asignadoController.eliminarAsignacion(asign);
					}
				}
				else{
					JOptionPane.showMessageDialog(null, "Ingrese un DNI de cientifico", "Información",JOptionPane.WARNING_MESSAGE);
				}
			}
			if (aux.getText().equals("<---"))
			{
				asignadoController.mostrarMenuPrincipal();
				dispose();
			} 
			
		}
			
	};
private JTextField ProyectoText;



public void setAsignadoController(AsignadoController asignadoController) {
	this.asignadoController = asignadoController;
}
}
