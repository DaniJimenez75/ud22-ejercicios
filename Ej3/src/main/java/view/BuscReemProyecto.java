package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.ProyectoController;
import model.dto.Cientifico;
import model.dto.Proyecto;

public class BuscReemProyecto extends JFrame {

	private JPanel contentPane;
	private JTextField nombreTexto;
	private JTextField horasTexto;
	private JButton actualizarButton;
	private ProyectoController proyectoController;
	JTextField idTexto;

	

	/**
	 * Create the frame.
	 */
	public BuscReemProyecto() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 710, 375);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("ID:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(216, 29, 28, 14);
		contentPane.add(lblNewLabel);
		

		
		JLabel lblNombre = new JLabel("NomApels:");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNombre.setBounds(143, 101, 68, 14);
		contentPane.add(lblNombre);

		
		actualizarButton = new JButton("Actualizar");
		actualizarButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		actualizarButton.setBounds(232, 234, 180, 70);
		actualizarButton.addActionListener(buscarReemplazar);
		actualizarButton.setEnabled(false);
		contentPane.add(actualizarButton);
		
		JButton buscarButton = new JButton("Buscar por ID");
		buscarButton.setFont(new Font("Tahoma", Font.PLAIN, 17));
		buscarButton.setBounds(353, 29, 146, 20);
		buscarButton.addActionListener(buscarReemplazar);
		contentPane.add(buscarButton);
		
		JButton btnNewButton_1 = new JButton("<---");
		btnNewButton_1.setBounds(12, 13, 76, 25);
		btnNewButton_1.addActionListener(buscarReemplazar);
		contentPane.add(btnNewButton_1);

		
		nombreTexto = new JTextField();
		nombreTexto.setBounds(221, 100, 86, 20);
		contentPane.add(nombreTexto);
		nombreTexto.setColumns(10);
		
		horasTexto = new JTextField();
		horasTexto.setColumns(10);
		horasTexto.setBounds(394, 100, 86, 20);
		contentPane.add(horasTexto);
		
		JLabel lblHoras = new JLabel("Horas:");
		lblHoras.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblHoras.setBounds(335, 101, 68, 14);
		contentPane.add(lblHoras);
		
		idTexto = new JTextField();
		idTexto.setColumns(10);
		idTexto.setBounds(254, 28, 86, 20);
		contentPane.add(idTexto);
	}
	
ActionListener buscarReemplazar = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			switch (aux.getText()) {
			case "<---":
				proyectoController.mostrarMenuPrincipal();
				dispose();
				break;
			case "Buscar por ID":
				buscarProyecto();
				break;
				
			case "Actualizar":
				modificarProyecto();
				break;
			}
			
		}
	};
	
	private void buscarProyecto() {
		Proyecto miProyecto = new Proyecto();
			if(idTexto.getText().length()>0) {
				miProyecto = proyectoController.buscarProyecto(idTexto.getText());
				if(miProyecto != null) {
					idTexto.setEnabled(false);
					nombreTexto.setEnabled(true);
					nombreTexto.setText(miProyecto.getNombre());
					horasTexto.setEnabled(true);
					horasTexto.setText(Integer.toString(miProyecto.getHoras()));
					actualizarButton.setEnabled(true);

				}else JOptionPane.showMessageDialog(this.contentPane, "Usuario no encontrado.");
			}
	}
	
	private void modificarProyecto() {
		Proyecto miProyecto = new Proyecto();
			
		//Guardo nueva info
		miProyecto.setId(idTexto.getText());
		miProyecto.setNombre(nombreTexto.getText());
		miProyecto.setHoras(Integer.parseInt(horasTexto.getText()));



		
		//Actualizo datos
		proyectoController.modificarProyecto(miProyecto);
		if(proyectoController.getProyectoServ().modificaProyecto) {
			nombreTexto.setEnabled(false);
			nombreTexto.setText("");
			horasTexto.setEnabled(false);
			horasTexto.setText("");
			idTexto.setEnabled(true);
			actualizarButton.setEnabled(false);
		}
		
		
	}

	public void setProyectoController(ProyectoController proyectoController) {
		this.proyectoController = proyectoController;
	}
	

}
