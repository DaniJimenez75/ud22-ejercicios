package Team1.Ej3;

import controller.*;
import model.service.*;
import view.*;

/**
 * Hello world!
 *
 */
public class App 
{
	CientificoServ miCientificoServ;
	ProyectoServ miProyectoServ;
	AsignadoServ miAsignacionServ;
	
	CientificoController miCientificoController;
	ProyectoController miProyectoController;
	AsignadoController miAsignacionController;
	
	
	BuscReemCientifico winConsultaCientifico;
	BuscReemProyecto winConsultaProyecto;
	BuscReemAsignacion winConsultaAsignacion;
	CrearCientifico winCrearCientifico;
	CrearProyecto winCrearProyecto;
	CrearAsignacion winCrearAsignacion;
	EliminarCientifico winEliminarCientifico;
	EliminarProyecto winEliminarProyecto;
	EliminarAsignacion winEliminarAsignacion;
	MenuPrincipal winPrincipal;
	
	
	
	/**
	 * @param args
	 */
    public static void main( String[] args )
    {
        App miPrincipal= new App();
        miPrincipal.iniciar();
    }

    /**
	 * Permite instanciar todas las clases con las que trabaja
	 * el sistema
	 */
	private void iniciar() {
		//Instanciar clases
		miCientificoController = new CientificoController();
		miCientificoServ = new CientificoServ();
		winConsultaCientifico = new BuscReemCientifico();
		winEliminarCientifico = new EliminarCientifico();
		winCrearCientifico = new CrearCientifico();
		
		miProyectoController = new ProyectoController();
		miProyectoServ = new ProyectoServ();
		winConsultaProyecto = new BuscReemProyecto();
		winCrearProyecto = new CrearProyecto();
		winEliminarProyecto = new EliminarProyecto();
		
		miAsignacionController = new AsignadoController();
		miAsignacionServ = new AsignadoServ();
		winConsultaAsignacion = new BuscReemAsignacion();
		winCrearAsignacion = new CrearAsignacion();
		winEliminarAsignacion = new EliminarAsignacion();
		
		winPrincipal = new MenuPrincipal();
		
		
		//Establecer relaciones entre clases
		miCientificoServ.setCientificoController(miCientificoController);
		winPrincipal.setCientificoController(miCientificoController);
		winConsultaCientifico.setCientificoController(miCientificoController);
		winCrearCientifico.setCientificoController(miCientificoController);
		winEliminarCientifico.setCientificoController(miCientificoController);
		
		miProyectoServ.setProyectoController(miProyectoController);
		winPrincipal.setProyectoController(miProyectoController);
		winConsultaProyecto.setProyectoController(miProyectoController);
		winCrearProyecto.setProyectoController(miProyectoController);
		winEliminarProyecto.setProyectoController(miProyectoController);
		
		miAsignacionServ.setAsignacionController(miAsignacionController);
		winPrincipal.setAsignadoController(miAsignacionController);
		winConsultaAsignacion.setAsignadoController(miAsignacionController);
		winCrearAsignacion.setAsignadoController(miAsignacionController);
		winEliminarAsignacion.setAsignadoController(miAsignacionController);
		
		//Establecer relaciones con clase controller
		miCientificoController.setCientificoServ(miCientificoServ);
		miCientificoController.setBuscarReemplazar(winConsultaCientifico);
		miCientificoController.setCrear(winCrearCientifico);
		miCientificoController.setEliminar(winEliminarCientifico);
		miCientificoController.setPrincipal(winPrincipal);
		
		miProyectoController.setProyectoServ(miProyectoServ);
		miProyectoController.setBuscarReemplazar(winConsultaProyecto);
		miProyectoController.setCrear(winCrearProyecto);
		miProyectoController.setEliminar(winEliminarProyecto);
		miProyectoController.setPrincipal(winPrincipal);
		
		miAsignacionController.setAsignadoServ(miAsignacionServ);
		miAsignacionController.setBuscarReemplazar(winConsultaAsignacion);
		miAsignacionController.setCrear(winCrearAsignacion);
		miAsignacionController.setEliminar(winEliminarAsignacion);
		miAsignacionController.setPrincipal(winPrincipal);
		
		winPrincipal.setVisible(true);
		
		
		
	}
}
