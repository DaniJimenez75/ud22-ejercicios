package controller;


import model.dto.Cientifico;
import model.service.CientificoServ;
import view.BuscReemCientifico;
import view.CrearCientifico;
import view.EliminarCientifico;
import view.MenuPrincipal;

public class CientificoController {

	private CientificoServ cientificoServ;
	private BuscReemCientifico buscarReemplazar;
	private CrearCientifico crear;
	private EliminarCientifico eliminar;
	private MenuPrincipal principal;
	/**
	 * @return the cientificoServ
	 */
	public CientificoServ getCientificoServ() {
		return cientificoServ;
	}
	/**
	 * @param cientificoServ the cientificoServ to set
	 */
	public void setCientificoServ(CientificoServ cientificoServ) {
		this.cientificoServ = cientificoServ;
	}
	/**
	 * @return the buscarReemplazar
	 */
	public BuscReemCientifico getBuscarReemplazar() {
		return buscarReemplazar;
	}
	/**
	 * @param buscarReemplazar the buscarReemplazar to set
	 */
	public void setBuscarReemplazar(BuscReemCientifico buscarReemplazar) {
		this.buscarReemplazar = buscarReemplazar;
	}
	/**
	 * @return the crear
	 */
	public CrearCientifico getCrear() {
		return crear;
	}
	/**
	 * @param crear the crear to set
	 */
	public void setCrear(CrearCientifico crear) {
		this.crear = crear;
	}
	/**
	 * @return the eliminar
	 */
	public EliminarCientifico getEliminar() {
		return eliminar;
	}
	/**
	 * @param eliminar the eliminar to set
	 */
	public void setEliminar(EliminarCientifico eliminar) {
		this.eliminar = eliminar;
	}
	/**
	 * @return the principal
	 */
	public MenuPrincipal getPrincipal() {
		return principal;
	}
	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(MenuPrincipal principal) {
		this.principal = principal;
	}
	
	//Hace visible las vstas de Crear, Buscar y actualizar, Eliminar
	public void mostrarMenuPrincipal() {
		principal.setVisible(true);
	}
	
	public void mostrarVentanaCreacion() {
		crear.setVisible(true);
	}
	public void mostrarVentanaBusActu() {
		buscarReemplazar.setVisible(true);
	}
	public void mostrarVentanaEliminar() {
		eliminar.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
	public void registrarCientifico(Cientifico miCientifico) {
		cientificoServ.validarRegistro(miCientifico);
	}
	
	public Cientifico buscarCientifico(String codigo) {
		return cientificoServ.validarConsulta(codigo);
	}
	
	public void modificarCientifico(Cientifico miCientifico) {
		cientificoServ.validarModificacion(miCientifico);
	}
	
	public void eliminarCientifico(String codigo) {
		cientificoServ.validarEliminacion(codigo);
	}
}
