package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.ClientController;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextPane;
import javax.swing.JLabel;

public class MenuPrincipal extends JFrame {

	private ClientController clienteController;
	private JPanel contentPane;

	
	public MenuPrincipal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 612, 546);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton menuCrear = new JButton("Crear");
		menuCrear.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuCrear.setBounds(146, 145, 324, 70);
		menuCrear.addActionListener(nuevaVentana);
		contentPane.add(menuCrear);
		
		JButton menuEliminar = new JButton("Eliminar");
		menuEliminar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuEliminar.setBounds(146, 341, 324, 70);
		menuEliminar.addActionListener(nuevaVentana);
		contentPane.add(menuEliminar);
		
		JLabel lblNewLabel = new JLabel("Selecciona una opción");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel.setBounds(212, 72, 229, 25);
		contentPane.add(lblNewLabel);
		
		JLabel lblGestinDeBd = new JLabel("GESTIÓN DE BD");
		lblGestinDeBd.setFont(new Font("Rockwell Condensed", Font.PLAIN, 22));
		lblGestinDeBd.setBounds(241, 13, 229, 25);
		contentPane.add(lblGestinDeBd);
		
		JButton menuBuscarActualizar = new JButton("Buscar y Actualizar");
		menuBuscarActualizar.setFont(new Font("Tahoma", Font.PLAIN, 20));
		menuBuscarActualizar.setBounds(146, 239, 324, 70);
		menuBuscarActualizar.addActionListener(nuevaVentana);
		contentPane.add(menuBuscarActualizar);
	}

	ActionListener nuevaVentana = new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JButton aux = (JButton) e.getSource();
			switch(aux.getText()) {
			case "Crear":
				dispose();
				clienteController.mostrarVentanaCreacion();
				break;
				
			case "Eliminar":
				dispose();
				clienteController.mostrarVentanaEliminar();
				break;
				
			case "Buscar y Actualizar":
				dispose();
				clienteController.mostrarVentanaBusActu();
				break;
			}
		}
	};
	
	
	
	/**
	 * @param clienteController the clienteController to set
	 */
	public void setClienteController(ClientController clienteController) {
		this.clienteController = clienteController;
	}

	
}
