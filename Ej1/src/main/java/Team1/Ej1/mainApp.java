package Team1.Ej1;

import controller.ClientController;
import model.service.ClienteServ;
import views.BuscarActualizar;
import views.Crear;
import views.Eliminar;
import views.MenuPrincipal;


public class mainApp 
{
	ClienteServ miclienteServ;
	BuscarActualizar ventanaBuscarActualizar;
	Crear ventanaCrear;
	Eliminar ventanaEliminar;
	MenuPrincipal menuPrincipal;
	ClientController clienteController;
	
	
	/**
	 * @param args
	 */
    public static void main( String[] args )
    {
        mainApp miPrincipal= new mainApp();
        miPrincipal.iniciar();
    }

    /**
	 * Permite instanciar todas las clases con las que trabaja
	 * el sistema
	 */
	private void iniciar() {
		//Instanciar clases
		clienteController = new ClientController();
		miclienteServ = new ClienteServ();
		menuPrincipal = new MenuPrincipal();
		ventanaCrear = new Crear();
		ventanaBuscarActualizar = new BuscarActualizar();
		ventanaEliminar = new Eliminar();
		
		//Establecer relaciones entre clases
		miclienteServ.setClienteController(clienteController);
		menuPrincipal.setClienteController(clienteController);
		ventanaCrear.setClienteController(clienteController);
		ventanaBuscarActualizar.setClienteController(clienteController);
		ventanaEliminar.setClienteController(clienteController);
		
		//Establecer relaciones con clase controller
		clienteController.setClienteServ(miclienteServ);
		clienteController.setCrear(ventanaCrear);
		clienteController.setBuscarActualizar(ventanaBuscarActualizar);
		clienteController.setEliminar(ventanaEliminar);
		clienteController.setMenuPrincipal(menuPrincipal);
		
		menuPrincipal.setVisible(true);
		
		
		
	}
}
