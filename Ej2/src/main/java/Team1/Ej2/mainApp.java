package Team1.Ej2;

import controller.ClientController;
import controller.VideoController;
import model.service.ClienteServ;
import model.service.VideoServ;
import views.BuscarActualizar;
import views.BuscarActualizarVideo;
import views.Crear;
import views.CrearVideo;
import views.Eliminar;
import views.EliminarVideo;
import views.MenuPrincipal;


public class mainApp 
{
	ClienteServ miclienteServ;
	BuscarActualizar ventanaBuscarActualizar;
	Crear ventanaCrear;
	Eliminar ventanaEliminar;
	MenuPrincipal menuPrincipal;
	ClientController clienteController;
	VideoController videoController;
	VideoServ mivideoServ;
	BuscarActualizarVideo ventanaBuscarActualizarVideo;
	CrearVideo ventanaCrearVideo;
	EliminarVideo ventanaEliminarVideo;
	
	/**
	 * @param args
	 */
    public static void main( String[] args )
    {
        mainApp miPrincipal= new mainApp();
        miPrincipal.iniciar();
    }

    /**
	 * Permite instanciar todas las clases con las que trabaja
	 * el sistema
	 */
	private void iniciar() {
		//Instanciar clases
		clienteController = new ClientController();
		miclienteServ = new ClienteServ();
		menuPrincipal = new MenuPrincipal();
		ventanaCrear = new Crear();
		ventanaBuscarActualizar = new BuscarActualizar();
		ventanaEliminar = new Eliminar();
		
		videoController = new VideoController();
		mivideoServ = new VideoServ();
		ventanaBuscarActualizarVideo = new BuscarActualizarVideo();
		ventanaCrearVideo = new CrearVideo();
		ventanaEliminarVideo = new EliminarVideo();
		
		
		
		
		//Establecer relaciones entre clases
		miclienteServ.setClienteController(clienteController);
		menuPrincipal.setClienteController(clienteController);
		ventanaCrear.setClienteController(clienteController);
		ventanaBuscarActualizar.setClienteController(clienteController);
		ventanaEliminar.setClienteController(clienteController);
		
		mivideoServ.setVideoController(videoController);
		menuPrincipal.setVideoController(videoController);
		ventanaCrearVideo.setVideoController(videoController);
		ventanaBuscarActualizarVideo.setVideoController(videoController);
		ventanaEliminarVideo.setVideoController(videoController);
		
			
		
		//Establecer relaciones con clase controller
		clienteController.setClienteServ(miclienteServ);
		clienteController.setCrear(ventanaCrear);
		clienteController.setBuscarActualizar(ventanaBuscarActualizar);
		clienteController.setEliminar(ventanaEliminar);
		clienteController.setMenuPrincipal(menuPrincipal);
		
		videoController.setVideoServ(mivideoServ);
		videoController.setBuscarActualizar(ventanaBuscarActualizarVideo);
		videoController.setCrear(ventanaCrearVideo);
		videoController.setEliminar(ventanaEliminarVideo);
		videoController.setMenuPrincipal(menuPrincipal);
		
		
		
		menuPrincipal.setVisible(true);
		
		
		
	}
}
