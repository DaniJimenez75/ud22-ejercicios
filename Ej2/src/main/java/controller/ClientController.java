package controller;

import model.dto.Cliente;
import model.service.ClienteServ;
import views.BuscarActualizar;
import views.Crear;
import views.Eliminar;
import views.MenuPrincipal;

public class ClientController {
	
	private ClienteServ clienteServ;
	private BuscarActualizar buscarActualizar;
	private Crear crear;
	private Eliminar eliminar;
	private MenuPrincipal menuPrincipal;
	/**
	 * @return the clienteServ
	 */
	public ClienteServ getClienteServ() {
		return clienteServ;
	}
	/**
	 * @param clienteServ the clienteServ to set
	 */
	public void setClienteServ(ClienteServ clienteServ) {
		this.clienteServ = clienteServ;
	}
	/**
	 * @return the buscarActualizar
	 */
	public BuscarActualizar getBuscarActualizar() {
		return buscarActualizar;
	}
	/**
	 * @param buscarActualizar the buscarActualizar to set
	 */
	public void setBuscarActualizar(BuscarActualizar buscarActualizar) {
		this.buscarActualizar = buscarActualizar;
	}
	/**
	 * @return the crear
	 */
	public Crear getCrear() {
		return crear;
	}
	/**
	 * @param crear the crear to set
	 */
	public void setCrear(Crear crear) {
		this.crear = crear;
	}
	/**
	 * @return the eliminar
	 */
	public Eliminar getEliminar() {
		return eliminar;
	}
	/**
	 * @param eliminar the eliminar to set
	 */
	public void setEliminar(Eliminar eliminar) {
		this.eliminar = eliminar;
	}
	/**
	 * @return the menuPrincipal
	 */
	public MenuPrincipal getMenuPrincipal() {
		return menuPrincipal;
	}
	/**
	 * @param menuPrincipal the menuPrincipal to set
	 */
	public void setMenuPrincipal(MenuPrincipal menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}
	
	
	//Hace visible las vstas de Crear, Buscar y actualizar, Eliminar
	public void mostrarMenuPrincipal() {
		menuPrincipal.setVisible(true);
	}
	
	public void mostrarVentanaCreacion() {
		crear.setVisible(true);
	}
	public void mostrarVentanaBusActu() {
		buscarActualizar.setVisible(true);
	}
	public void mostrarVentanaEliminar() {
		eliminar.setVisible(true);
	}
	
	//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
		public void registrarCliente(Cliente miCiente) {
			clienteServ.validarRegistro(miCiente);
		}
		
		public Cliente buscarCliente(String codigoCliente) {
			return clienteServ.validarConsulta(codigoCliente);
		}
		
		public void modificarCliente(Cliente miCliente) {
			clienteServ.validarModificacion(miCliente);
		}
		
		public void eliminarCliente(String codigo) {
			clienteServ.validarEliminacion(codigo);
		}
	
}
